var users = [
  {
      first_name: "Josephine",
      last_name: "Robinson",
      birthday: "1996-09-26",
  },
  {
      first_name: "Dean",
      last_name: "Long",
      birthday: "1984-10-23",
  },
  {
      first_name: "Sonia",
      last_name: "Holmes",
      birthday: "1958-06-21",
  },
  {
      first_name: "June",
      last_name: "Mcdonalid",
      birthday: "1960-05-06",
  },
  {
      first_name: "Ella",
      last_name: "Lane",
      birthday: "1991-12-11",
  },
  {
      first_name: "Felecia",
      last_name: "Stone",
      birthday: "1958-04-21",
  },
  {
      first_name: "Elmer",
      last_name: "George",
      birthday: "1987-12-10",
  }
];
